<?php

namespace App\Http\Controllers\V1;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Tymon\JWTAuth\JWTAuth;

class AuthUserController extends BaseController {
    //
    /**
     * @var JWTAuth
     */
    private $jwtAuth;

    protected $guard = 'user';


    /**
     * Create a new AuthController instance.
     *
     * @return void
     */
    public function __construct(JWTAuth $jwtAuth) {
        $this->middleware('auth:' . $this->guard, ['except' => ['login']]);
        $this->jwtAuth = $jwtAuth;
    }

    public function formatToken($token) {
        $response = [
            'access_token' => $token,
            'token_type'   => 'bearer',
            'expires_in'   => auth($this->guard)->factory()->getTTL() * 1000000000,
        ];

        return $response;
    }

    public function login(Request $request) {
        $data = $request->all();

        $callback = function () use ($data) {
            $token = auth($this->guard)->attempt($data);
            if (!$token) {
                $errors = ["user" => ["Invalid credentials. Please try again"]];
                return clientErrorResponse($errors);
            }

            return showResponse($this->formatToken($token));
        };

        return $this->exceptionHandler($callback);
    }


    /**
     * Log the user out (Invalidate the token).
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function logout() {

        auth($this->guard)->logout();

        return customResponse(204, 'Successfully logged out', [], true);

    }

    public function refresh() {
        return showResponse($this->formatToken(auth($this->guard)->refresh()));
    }


}
