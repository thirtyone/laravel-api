<?php



$router->resources([
   'samples' => 'SampleController'
]);

$router->group(['prefix' => "auth"], function ($router) {
    $router->post('login', 'AuthUserController@login');
    $router->post('logout', 'AuthUserController@logout');
    $router->post('refresh-token', 'AuthUserController@refresh');
});
